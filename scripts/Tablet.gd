tool
extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var down_path: NodePath
export var up_path: NodePath
export var lerp_factor: float = 0.2

onready var down: Spatial = get_node(down_path)
onready var up: Spatial = get_node(up_path)
var is_active = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func button():
	is_active = !is_active

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var target
	var target_origin
	if is_active:
		target = up.global_transform.basis
		target_origin = up.global_transform.origin
	elif not Engine.editor_hint:
		target = down.global_transform.basis
		target_origin = down.global_transform.origin
	else:
		target = global_transform.basis
		target_origin = global_transform.origin
	global_transform.basis.x = global_transform.basis.x.linear_interpolate(target.x, lerp_factor)
	global_transform.basis.y = global_transform.basis.y.linear_interpolate(target.y, lerp_factor)
	global_transform.basis.z = global_transform.basis.z.linear_interpolate(target.z, lerp_factor)
	global_transform.origin = global_transform.origin.linear_interpolate(target_origin, lerp_factor)
	pass
