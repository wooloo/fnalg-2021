extends Camera


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var min_x = -0.8
export var max_x = 0.8
export var min_y = 0.0
export var max_y = 0.0
export var speed = 0.4
onready var starting_rotation = rotation

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var pos = get_viewport().get_mouse_position()
	var size = get_viewport().get_visible_rect().size
	var norm_pos = pos/size
	var next_rot_x = 0
	var next_rot_y = 0
	if norm_pos.x < 0.2:
		next_rot_x += 1
	elif norm_pos.x > 0.8:
		next_rot_x -= 1
	if norm_pos.y < 0.2:
		next_rot_y += 1
	elif norm_pos.y > 0.8:
		next_rot_y -= 1
	var next_rot = rotation + (Vector3(next_rot_y, next_rot_x, 0) * delta * speed)
	var next_rot_diff = next_rot - starting_rotation
	if next_rot_diff.x > min_y and next_rot_diff.x < max_y:
		rotation.x = next_rot.x
	if next_rot_diff.y > min_x and next_rot_diff.y < max_x:
		rotation.y = next_rot.y
	
