extends Node

class_name ViewportForward

export var viewport: NodePath
export var collider: NodePath

func _ready():
	#get_node(collider).connect("mouse_entered", self, "_mouse_entered")
	#get_node(collider).connect("mouse_exited", self, "_mouse_exited")
	pass

var mouse_inside = false
func _mouse_entered():
	mouse_inside = true
func _mouse_exited():
	mouse_inside = false

func _input(event):
	if mouse_inside:
		if event is InputEventMouseMotion:
			pass
		else:
			get_node(viewport).input(event)
