extends Spatial

onready var anim = get_node("AnimationPlayer")
var open = false

export var close_anim = "Close"
export var open_anim = "Open"
export var register = true
export var register_temperature = false
export var consume = 0.2
export var effect_open = -0.2
export var effect_closed = 0.5

func _ready():
	button(0)
	if register:
		$"/root/GameStateSingleton".register_consumer(self)
	if register_temperature:
		$"/root/GameStateSingleton".register_affector(self)

func _process(delta):
	if $"/root/GameStateSingleton".get_power() < 0:
		if !open:
			anim.play(open_anim)
		open = true

func button(_index):
	if $"/root/GameStateSingleton".get_power() > 0:
		toggle_open()

func toggle_open():
	if open:
		anim.play(close_anim)
	else:
		anim.play(open_anim)
	open = not open

func get_enabled():
	return !open

func get_consumption():
	if !open:
		return consume
	else:
		return 0

func get_temperature_effect():
	if open:
		return effect_open
	else:
		return effect_closed
