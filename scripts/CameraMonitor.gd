extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var viewports: PoolStringArray;
export var starting_index = 0

onready var scr = get_node("screen")

var index = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	set_camera_index(starting_index)
	pass # Replace with function body.

func set_camera_index(tgt):
	index = tgt
	var vp = get_node(viewports[tgt])
	var mat = SpatialMaterial.new()
	mat.emission_enabled = true
	mat.emission_texture = vp.get_texture()
	scr.mesh.surface_set_material(0, mat)
	scr.material_override = mat

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func button(cam_index):
	if cam_index == 0:
		set_camera_index((index - 1) % len(viewports))
	elif cam_index == 1:
		set_camera_index((index + 1) % len(viewports))
