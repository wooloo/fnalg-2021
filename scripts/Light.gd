extends OmniLight

export var on_by_default = false;
export var register = true;
export var consume = 0.2;

func _ready():
	if not on_by_default:
		button(0)
	if register:
		$"/root/GameStateSingleton".register_consumer(self)

onready var default_energy = light_energy

var flickering = false

func _process(delta):
	if $"/root/GameStateSingleton".get_power() < 0:
		light_energy = 0
		flickering = false
		remove_and_skip()
	if rand_range(0,5) > 4.99 and light_energy > 0:
		button(0)
		flickering = true
	elif flickering:
		button(0)
		flickering = false
func button(_index):
	if light_energy > 0:
		light_energy = 0
	else:
		light_energy = default_energy

func get_enabled():
	return light_energy > 0

func get_consumption():
	if get_enabled():
		return consume
	else:
		return 0
