extends StaticBody


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var button_target: NodePath
export var index: int

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

var mouse_inside = false

func _on_button_mouse_entered():
	mouse_inside = true


func _on_button_mouse_exited():
	mouse_inside = false

var was_pressed = false

func _input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and self.mouse_inside:
		if event.is_pressed():
			if !was_pressed:
				get_node(button_target).button(index)
				was_pressed = true
		else:
			was_pressed = false
