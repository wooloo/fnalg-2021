extends Button

export var submenu_parent_path: NodePath
export var target_scene: PackedScene

func _pressed():
	var submenu_parent = get_node(submenu_parent_path)
	for i in submenu_parent.get_children():
		i.queue_free()
	submenu_parent.add_child(target_scene.instance())
