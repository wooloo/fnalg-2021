extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var time = 0.0
var seen = true
var enabled = false
onready var timer_scene: PackedScene = load("res://scenes/SpeedrunTimerDisplay.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if enabled:
		if !seen:
			get_node("/root/").add_child(timer_scene.instance())
		seen = false;
		time += delta

func get_time():
	seen = true
	return time

func start():
	time = 0.0
	enabled = true
