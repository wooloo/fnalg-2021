extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var chance = 0.4
# These locations and animations will be used for numbered states
export var location_marker_parent: NodePath
export var animations: PoolStringArray
export var left_door: NodePath
export var right_door: NodePath

# Called when the node enters the scene tree for the first time.
func _ready():
	$"/root/GameStateSingleton".call_deferred("register_animatronic", self)
	assume_state()
	pass # Replace with function body.

var state = 0

func assume_state():
	print(state)
	var goal = get_node(location_marker_parent).get_children()[state]
	global_transform.origin = goal.global_transform.origin
	rotation_degrees = goal.rotation_degrees
	$"./Animator".play(animations[state])

func activity():
	print("Should I move?")
	var test = randf() 
	print(test)
	if test < chance:
		print("Yes")
		match state:
			0:
				state = 1
			1:
				if randf() < 0.5:
					state = 2
				else:
					state = 4
			2:
				state = 3
			4:
				state = 5
			3:
				if get_node(left_door).get_enabled():
					print("Returned to the start...")
					state = 0
				else:
					print("Jumpscare!")
					state = 0
			5:
				if get_node(right_door).get_enabled():
					print("Returned to the start...")
					state = 0
				else:
					print("Jumpscare!")
					state = 0
		assume_state()
