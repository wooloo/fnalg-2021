extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var chance = 0.4
# These locations and animations will be used for numbered states
export var location_marker_parent: NodePath
export var animations: PoolStringArray
export var door: NodePath

# Called when the node enters the scene tree for the first time.
func _ready():
	$"/root/GameStateSingleton".call_deferred("register_animatronic", self)
	assume_state()
	pass # Replace with function body.

var state = 0

func assume_state():
	print(state)
	var goal = get_node(location_marker_parent).get_children()[state]
	global_transform.origin = goal.global_transform.origin
	rotation_degrees = goal.rotation_degrees
	$"./Animator".play(animations[state])

func activity():
	print("Should I move?")
	var test = randf() 
	print(test)
	if test < chance:
		print("Yes")
		match state:
			0:
				state = 1
			1:
				state = 2
			2:
				state = 3
			3:
				state = 4
			4:
				if get_node(door).get_enabled():
					print("Returned to the start...")
					state = 0
				else:
					state = 5
					print("Jumpscare!")
			5:
				state = 0
		assume_state()
