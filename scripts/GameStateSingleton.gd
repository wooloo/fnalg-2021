extends Node

var power_level = 100.0
var temperature = 60.0
var consumers = []
var affectors = []
var difficulty_levels = []
var animatronics = []
var activity_interval = 5.0
var activity_clock = 0.0

func _process(delta):
	for i in consumers:
		power_level -= i.get_consumption() * delta
	for i in affectors:
		temperature += i.get_temperature_effect() * delta
	if power_level > 100.0:
		power_level = 100.0
	activity_clock += delta
	if activity_clock > activity_interval:
		for i in animatronics:
			i.activity()
		activity_clock = 0.0

func init():
	power_level = 100.0
	temperature = 60.0
	consumers = []
	difficulty_levels = []
	animatronics = []
	activity_clock = 0.0

func register_consumer(consumer):
	consumers.append(consumer)

func register_affector(affector):
	affectors.append(affector)

func get_power():
	return power_level

func get_temperature():
	return temperature

func register_animatronic(animatronic):
	animatronics.append(animatronic)

func update_difficulty(index, level):
	while len(difficulty_levels) <= index:
		difficulty_levels.append(0)
	difficulty_levels[index] = level

func get_difficulty(index):
	return difficulty_levels[index]
