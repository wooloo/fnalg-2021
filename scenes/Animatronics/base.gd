extends Spatial

class_name AnimatronicBase

export var location_marker_parent: NodePath
export var animations: PoolStringArray
export var state = 0

func assume_state():
	print(state)
	var goal = get_node(location_marker_parent).get_children()[state]
	global_transform.origin = goal.global_transform.origin
	rotation_degrees = goal.rotation_degrees
	$"./Animator".play(animations[state])

func _ready():
	$"/root/GameStateSingleton".call_deferred("register_animatronic", self)
	assume_state()
